

## Uygulamayı çalıştırmak için

Bilgisayarında Node.js kurulu olması gerekiyor ve NPM'in de kurulu olması gerekiyor. 

Repo'nun ana dizinine girip aşağıdaki komutları çalıştırman yeterli.

# npm install

Çalıştırmak için ise;

# gulp

Otomatik olarak Google Chrome açılır ve sayfa çalışır.

