﻿
$(document).ready(function () {
   


    if (!$('#clearAllFilters').length > 0)
        $('.form-actions').append(" <input type='reset' value='Clear' class='btn blue' id='clearAllFilters' />");

    $('#clearAllFilters').click(function () {
        $('#GridFilterForm select').each(function () {
            $(this).val($('this option:first').val());
        });

        $('#GridFilterForm input[type=text]').each(function () {
            $(this).val("");
        });

        $('#GridFilterForm select').trigger("chosen:updated");

        $('#CustomFilter select').each(function () {
            $(this).val($('this option:first').val());
        });

        $('#CustomFilter input[type=text]').each(function () {
            $(this).val("");
        });

        $('#CustomFilter select').trigger("chosen:updated");
    })
})


function FilterGrid() {
    var o = {};
    var form = $("#GridFilterForm");
    var serialized = form.serializeArray();
    $.each(serialized, function () {
        //if (o[this.name]) {
        //    o[this.name] = o[this.name] + ',' + this.value || '';
        //} else {
        //    o[this.name] = this.value || '';
        //}
        if (o[this.name]) {
            if ((typeof (this.value) === "boolean") ||
                (typeof (this.value) === "string" && this.value != undefined && (this.value.toLowerCase() === "true" || this.value.toLowerCase() === "false"))) {
                o[this.name] = ((o[this.name] == "true") | (this.value == "true")) ? true : false;
            }
            else {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            }
        } else {
            o[this.name] = this.value || '';
        }

    });

    return o;

}

$("#GridFilterButton").click(function (e) {

    var onBeginFunction = $('#GridFilterForm').attr('data-ajax-begin');
    if (onBeginFunction != null) {
        var onBeginResponse = getFunction(onBeginFunction, []).apply(this, []);
        if (onBeginResponse==false) {
            return false;
        }
    }

    var grid = $("#" + gridViewId).data("kendoGrid");


    grid.bind("dataBound", function () {
        var successFunction = $('#GridFilterForm').attr('data-ajax-success');
        if (successFunction != null) {
            getFunction(successFunction, []).apply(this, []);
        }
    });

    grid.dataSource.read();

    var onCompleteFunction = $('#GridFilterForm').attr('data-ajax-complete');
    if (onCompleteFunction != null) {
        getFunction(onCompleteFunction, []).apply(this, []);
    }

   
    return false;    
});

function GridFormOnSuccess() {
    var gridIdSelector = "#" + gridViewId;
    $(gridIdSelector).data('kendoGrid').dataSource.read();
    $(gridIdSelector).data('kendoGrid').refresh();
}

