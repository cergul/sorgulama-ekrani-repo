$(document).ready(function () {
    handleAjaxMessages();
    if (isdefined("messageContext")) {
        displayMessage(messageContext.messages, messageContext.type);
    }
});

function handleAjaxMessages() {
    $(document).ajaxSuccess(function (event, request) {
        checkAndHandleMessageFromHeader(request);
    }).ajaxError(function (event, request) {
        if (request.statusText != "abort") {
            if (request.responseText != "") {
                var messageContext = $.parseJSON(request.responseText);
                displayMessage(messageContext, "error");
            }
        }
    });
}

function checkAndHandleMessageFromHeader(request) {
    var messagesRaw = request.getResponseHeader("X-Message");
    if (messagesRaw) {
        var messageContext = $.parseJSON(messagesRaw);
        displayMessage(messageContext, request.getResponseHeader("X-Message-Type"));
    }
}

function displayMessage(messageContext, messageType) {

    $.each(messageContext.Messages,
        function (i, message) {

            message = decode_utf8(message);

            //Checks Fow Tags in Message
            var fowRegexp = message.match(/<FOW>([\s\S]*?)<\/FOW>/im);
            if (fowRegexp && fowRegexp.length > 0) {
                message = fowRegexp[1];
            }

            var title = "";

            if (messageType == "success") {
                title = "Başarılı";
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "positionClass": "toast-top-full-width",
                    "onclick": null,
                    "showDuration": "400",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    //"extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                toastr.success(message);
                $("#toast-container > div").removeClass("toast-height");

            } else if (messageType == "warning") {
                title = "Uyarı";
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "positionClass": "toast-top-full-width",
                    "onclick": null,
                    "showDuration": "400",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    //"extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                toastr.warning(message);
                $("#toast-container > div").removeClass("toast-height");

            } else if (messageType == "error") {
                title = "Hata";
                toastr.options = toastrOptions;
                toastr.error(message);
                $("#toast-container > div").addClass("toast-height");
            } else {
                title = "Hata";
            }

        });
}

function ShowError(message) {

    toastr.options = toastrOptions;
    toastr.error(message);
    $("#toast-container > div").addClass("toast-height");
}

function ShowSuccess(message) {

    toastr.options = toastrOptions;
    toastr.options.timeOut = 4000;
    toastr.success(message);
    $("#toast-container > div").addClass("toast-height");
}

function isdefined(variable) {
    return (typeof (window[variable]) == "undefined") ? false : true;
}

function encode_utf8(s) {
    return unescape(encodeURIComponent(s));
}

function decode_utf8(s) {
    try {
        return decodeURIComponent(escape(s));
    } catch (e) {
        return s;
    }
}

var toastrOptions = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "rtl": false,
    "positionClass": "toast-top-center-screen",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": 300,
    "hideDuration": 1000,
    "timeOut": 0,
    "extendedTimeOut": 0,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};