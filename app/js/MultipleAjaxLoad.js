﻿;
(function($) {
    var MultipleLoad = {
        defaults: { dataType: "html" },

        ajax: function(options) {
            options.userSuccess = options.success;
            options.success = MultipleLoad.processResponse;
            var combinedOpts = $.extend({}, MultipleLoad.defaults, options);
            $.ajax(combinedOpts);
        },

        processResponse: function(data) {
            var options = this;
            $data = $(data);

            if (!$data.hasClass("ajax-response")) {
                return;
            }

            $data.find(".ajax-content").each(function() {
                var $content = $(this);
                var cmd = MultipleLoad.getCommand($content);
                var selector = MultipleLoad.getSelector($content);
                $(selector)[cmd]($content.html());
            });

            $data.find("script").each(function() { eval($(this).html()); });

            if (typeof options.userSuccess === "function") {
                options.userSuccess.apply(this, arguments);
            }

        },

        getCommand: function($content) {
            var cmd = $content.attr("title").split(" ")[0];
            if (cmd === "!update" || cmd.substring(0, 1) !== "!") return "html";
            if (cmd === "!appendTo") return "append";
            if (cmd === "!prependTo") return "prepend";
            return cmd.substring(1);
        },

        getSelector: function($content) {
            var title = $content.attr("title");
            return title.substring(title.indexOf(" ") + 1);
        }
    };

    $.MultipleLoad = MultipleLoad.ajax;
})(jQuery);