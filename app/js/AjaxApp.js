﻿var pageConfirmControls;
$("body").on('click', "a[actionTypeId = 'OpenDialogFormOnGrid']", function (e) {

    e.preventDefault();

    ajaxApp.init(this, { showDialog: true, refreshGrid: true });

    var IsConfirm = true;

    if ((this.href.toLowerCase().indexOf("delete") >= 0)) {
        IsConfirm = confirm('Are you sure you wish to delete this record?');
    }

    if (pageConfirmControls) {
        for (var i = 0; i < pageConfirmControls.length; i++) {
            if ((this.href.toLowerCase().indexOf(pageConfirmControls[i].key) >= 0)) {
                IsConfirm = confirm(pageConfirmControls[i].message);
            }
        }
    }

    if (IsConfirm) {

        ajaxApp.createAjax({});
    }


});


$("button[id='DefaultCancelButton']").on('click', function (e) {

    e.preventDefault();

    var url = $(this).attr("redirect-url");

    if (url != undefined) {
        location.href = url;
    }

});

$("body").on('click', "a[actionTypeId='OpenDialogFormOnGridByFilter']", function (e) {

    e.preventDefault();

    var form = $("#GridFilterForm");

    var formObject = ajaxApp.serializeObjectForGrid(form);

    ajaxApp.init(this, { showDialog: true, refreshGrid: false, data: formObject });


    //if ((this.href.indexOf("create") == -1) && ajaxApp.linkParams.selectedId == 0) {
    //    alert(jsResource.SelectRowOnGridErrMsg);
    //}
    //else {
    ajaxApp.createAjax({});


});

$("body").on('click', "a[actionTypeId='DirectActionOnGrid']", function (e) {
    e.preventDefault();

    ajaxApp.init(this, { showDialog: false, refreshGrid: true });

    var IsConfirm = true;

    if ((this.href.toLowerCase().indexOf("delete") >= 0)) {
        IsConfirm = confirm('Are you sure you wish to delete this record?');
    }

    if (IsConfirm) {

        ajaxApp.createAjax({});
    }
});

$("body").on('click', "a[actionTypeId='DirectActionOnButton']", function (e) {

    e.preventDefault();

    ajaxApp.init(this, { showDialog: false, refreshGrid: false, UpdateTarget: true });

    if (ajaxApp.linkParams.routeValues != null && ajaxApp.linkParams.routeValues != "") {

        var parameterNames = ajaxApp.linkParams.routeValues.split(",");

        var parameters = {};

        for (var i = 0; i < parameterNames.length; i++) {
            parameters[parameterNames[i]] = $("#" + parameterNames[i]).val();
        }

        ajaxApp.appParams.data = parameters;
    }

    var IsConfirm = true;

    if ((this.href.toLowerCase().indexOf("delete") >= 0)) {
        IsConfirm = confirm('Are you sure you wish to delete this record?');
    }

    if (IsConfirm) {

        ajaxApp.createAjax({});
    }
});

$("body").on('click', "a[actionTypeId = 'OpenDialogFormOnPage']", function (e) {

    e.preventDefault();

    ajaxApp.init(this, { showDialog: true, refreshGrid: false, IsFormDialog: true });

    if (ajaxApp.linkParams.routeValues != null) {

        var parameterNames = ajaxApp.linkParams.routeValues.split(",");

        var parameters = {};

        for (var i = 0; i < parameterNames.length; i++) {

            parameters[parameterNames[i]] = $("#" + parameterNames[i]).val();
        }

        ajaxApp.appParams.data = parameters;
    }
    //customdata fonsiyonu Taner
    if ($(this).attr("linkextradatafunction")) {
        if (ajaxApp.appParams.data === undefined) {
            ajaxApp.appParams.data = {};
        }
        var extraData = eval($(this).attr("linkextradatafunction") + "();");
        $.extend(ajaxApp.appParams.data, extraData);
    }



    ajaxApp.createAjax({});
});

$("body").on('click', "a[actionTypeId='RedirectToAction']", function (e) {


    e.preventDefault();
    ajaxApp.init(this, { showDialog: false, refreshGrid: false });

    //if ((this.href.indexOf("add") == -1) && ((this.href.indexOf("exportto") == -1)) && ajaxApp.linkParams.selectedId == 0) {
    //    alert(jsResource.SelectRowOnGridErrMsg);
    //}
    //else {
    location.href = ajaxApp.linkParams.url + location.search;
    //}
});

$("body").on('click', "a[actionTypeId='ExportToDocument']", function (e) {

    e.preventDefault();
    var form = $("#GridFilterForm");
    var formObject = ajaxApp.serializeObjectForGrid(form);

    var queryString = "?";

    for (var value in formObject) {
        queryString += value + "=" + formObject[value] + "&";
    }

    location.href = this.href + queryString;
});

$("body").on('click', "a[actionTypeId='UpdateTargetIdBySelectedGridRow']", function (e) {

    e.preventDefault();

    ajaxApp.init(this, { showDialog: false, refreshGrid: false, UpdateTarget: true });

    //if ((this.href.indexOf("create") == -1) && ajaxApp.linkParams.selectedId == 0) {
    //    alert(jsResource.SelectRowOnGridErrMsg);
    //}
    //else {
    ajaxApp.createAjax({});
    //}

});

$('body').on('click', 'form button[data-action]', function (e) {


    this.form.action = $(this).data('action');


    if ($(this).data('updatedtargetid') !== undefined) {
        $(this.form).attr('data-ajax-update', "#" + $(this).data('updatedtargetid'));
    }
});

$("body").on('click', '[data-toggle="tabcontent"]', function (e) {


    e.preventDefault();
    var $this = $(this);
    $this.tab('show');
    return false;
});

$("body").on('click', '[data-toggle="tabajax"]', function (e) {

    //alert("ok");
    var $this = $(this);
    var targ = $this.attr('data-target');

    if ($(targ).html() == "") {
        var loadurl = $this.attr('href');

        $.get(loadurl, function (data) {
            $(targ).html(data);
        });
    }

    $this.tab('show');

    return false;
});

$('body').on('change', '#CheckAll', function () {
    $(".checkboxFilter").find(':checkbox').prop("checked", this.checked);
});

function FormGlobalSuccess() {
    var idSelector = "#" + ajaxApp.linkParams.updateTargetId;
    $(idSelector).modal('hide');
}

function getFunction(code, argNames) {
    var fn = window, parts = (code || "").split(".");
    while (fn && parts.length) {
        fn = fn[parts.shift()];
    }
    if (typeof (fn) === "function") {
        return fn;
    }
    argNames.push(code);
    return Function.constructor.apply(null, argNames);
}

var ajaxApp = {
    linkParams: {},
    linkExtraDataFunction: {},
    link: null,
    popup: null,
    appParams: {},
    IsBlockUIEnabled: true,
    init: function (link, params) {
        this.link = link;
        this.setLinkParameters(link);
        this.appParams = params;
        this.appParams.abort = false;
    },
    Ajax: function (params) {
        var a = document.createElement('a');

        $(a)
            .attr('href', params.url)
            .attr('method', params.method)
            .attr("dataupdatetargetid", params.dataupdatetargetid)
            .attr("datatitle", params.datatitle)
            .attr("dataRouteValue", params.dataRouteValue)
            .attr("OnErrorFunc", params.OnErrorFunc)
            .attr("OnSuccessFunc", params.OnSuccessFunc)
            .attr("OnBeforeFunc", params.OnBeforeFunc)
            .attr("OnCompleteFunc", params.OnCompleteFunc)
            ;

        if (params.dataupdatetargetid) {
            params.UpdateTarget = true;
        }

        ajaxApp.init(a,
            {
                showDialog: params.showDialog,
                refreshGrid: params.refreshGrid,
                data: params.data,
                UpdateTarget: params.UpdateTarget,
                method: params.method
            });

        ajaxApp.createAjax({});
    },
    createAjax: function () {

        if (this.appParams.abort == true) return;

        var url = this.linkParams.url;

        //alert(ajaxApp.appParams.method);


        $.ajax({
            url: url,
            data: ajaxApp.appParams.data,
            type: this.appParams.method || 'GET',
            cache: false,
            success: function (data) {
                if (ajaxApp.appParams.showDialog) {
                    ajaxApp.showDialog(data);
                } else if (ajaxApp.appParams.refreshGrid) {
                    var gridIdSelector = "#" + ajaxApp.linkParams.gridViewId;
                    $(gridIdSelector).data('kendoGrid').dataSource.read();
                    $(gridIdSelector).data('kendoGrid').refresh();
                }
                else if (ajaxApp.appParams.UpdateTarget) {
                    $("#" + ajaxApp.linkParams.updateTargetId).html("");
                    $("#" + ajaxApp.linkParams.updateTargetId).html(data);
                    $.validator.unobtrusive.parse($("#" + ajaxApp.linkParams.updateTargetId));
                }

                if (ajaxApp.linkParams.OnSuccessFunc != null) {
                    getFunction(ajaxApp.linkParams.OnSuccessFunc, []).apply(this, []);
                }
            },
            beforeSend: function () {
                if (ajaxApp.linkParams.OnBeforeFunc != null) {
                    getFunction(ajaxApp.linkParams.OnBeforeFunc, []).apply(this, []);
                }
            },
            complete: function () {
                if (ajaxApp.linkParams.OnCompleteFunc != null) {
                    getFunction(ajaxApp.linkParams.OnCompleteFunc, []).apply(this, []);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var title = "Hata";
                toastr.error(thrownError);
                ajaxApp.unblockUI();
                if (ajaxApp.linkParams.OnErrorFunc != null) {
                    getFunction(ajaxApp.linkParams.OnErrorFunc, []).apply(this, []);
                }
            }


        });
    },
    showDialog: function (data) {

        //var title = this.linkParams.title;

        var modalIdSelector = "#" + ajaxApp.linkParams.updateTargetId;


        //alert(modalIdSelector);
        //alert(gridIdSelector);

        if ($(modalIdSelector).length == 0) {
            $("<div></div>")
                .attr('role', 'dialog')
                //.attr('tabindex', '-1')
                .attr('class', 'modal fade modal-fullscreen')
                .attr('id', ajaxApp.linkParams.updateTargetId)
                .appendTo('body');
        }



        $(modalIdSelector).html("");

        $(modalIdSelector).html(data);

        $(modalIdSelector).on('hidden.bs.modal', function () {

            $(this).remove();

            if (ajaxApp.appParams.refreshGrid) {
                ajaxApp.RefreshGrid(ajaxApp.linkParams.gridViewId);
            }

        });

        $(modalIdSelector).on('hidden.bs.modal', function (event) {
            if ($('.modal:visible').length) {
                $('body').addClass('modal-open');
            }
        });

        $(modalIdSelector).on('shown.bs.modal', function () {
            if (chosenEnabled) {
                $('.chosen-select', this).chosen('destroy').chosen({ case_sensitive_search: false, width: "95%", search_contains: true });
            }
        });

        $(modalIdSelector).modal('show');
    },
    setLinkParameters: function (link) {

        this.linkParams.gridViewId = $(link).attr("gridViewId");
        this.linkParams.title = $(link).attr("datatitle");
        this.linkParams.updateTargetId = $(link).attr("dataupdatetargetid");
        this.linkParams.routeValues = $(link).attr("dataroutevalues");
        this.linkParams.url = this.getRouteUrlFromGrid(link.href, this.linkParams.routeValues);
        this.linkParams.selectedId = this.getGridSelectedId();
        this.linkParams.OnSuccessFunc = $(link).attr("OnErrorFunc");
        this.linkParams.OnSuccessFunc = $(link).attr("OnSuccessFunc");
        this.linkParams.OnBeforeFunc = $(link).attr("OnBeforeFunc");
        this.linkParams.OnCompleteFunc = $(link).attr("OnCompleteFunc");
        this.linkParams.method = $(link).attr("method");


    },
    addParameterFromHtmlElement: function (id) {
        var parameter = $("#" + id).val();
        ajaxApp.linkParams.url = ajaxApp.linkParams.url + "?" + id + "=" + parameter;
    },
    closeDialog: function () {
        var idSelector = "#" + ajaxApp.linkParams.updateTargetId;
        $(idSelector).modal('hide');
    },
    RefreshGrid: function (gridId) {
        var gridIdSelector = "#" + gridId;
        var grid = $(gridIdSelector).data('kendoGrid');
        if (grid != null) {
            $(gridIdSelector).data('kendoGrid').dataSource.read();
            $(gridIdSelector).data('kendoGrid').refresh();
        }
    },
    addParameterFromGridCheckboxes: function () {
        var $ids = $(':checked');
        if ($ids.length < 1) {
            alert(jsResource.CheckGridRows);
            this.appParams.abort = true;
            return;
        }
        this.appParams.data = $ids;
    },
    getRouteUrlFromGrid: function (rawUrl, routeValues) {

        //var queryObj = this.getQueryStringObject()
        //var selectedId = this.getGridSelectedId();
        var routeUrl = rawUrl;

        //if (selectedId > 0 || selectedId == -1) {
        //    routeUrl = routeUrl + "/" + selectedId;
        //}

        //if (typeof routeValues !== "undefined" && routeValues !== "") {
        //    if (selectedId != 0 && selectedId != -5) {
        //        queryObj[routeValues] = selectedId;
        //        routeUrl = routeUrl + "?" + routeValues + "=" + queryObj[routeValues];
        //    }
        //}
        return routeUrl;
    },
    getQueryStringObject: function () {

        var querystring = location.search.replace('?', '').split('&');
        var queryObj = {};
        for (var i = 0; i < querystring.length; i++) {
            var name = querystring[i].split('=')[0];
            var value = querystring[i].split('=')[1];
            queryObj[name] = value;
        }

        return queryObj;
    },
    getGridSelectedId: function () {

        var grid = this.getGrid();
        if (grid != null) {

            var row = grid.select();
            var data = grid.dataItem(row);
            if (row != null) {
                var uid = row.data("uid");
                var selectedId = $('[data-uid="' + uid + '"] td:first').text();
                return selectedId;
            }
        }

        return -5;
    },
    emptyRowMsg: function () {
        alert(jsResource.SelectRecordMsg);
    },
    getGrid: function () {
        if (this.linkParams.gridViewId !== undefined && this.linkParams.gridViewId !== "") {
            return $("#" + this.linkParams.gridViewId).data("kendoGrid");
        } else {
            return $(".k-grid").data("kendoGrid");
        }
    },
    getGridById: function (dataGridId) {
        return $("#" + dataGridId).data('kendoGrid');
    },
    filterGridByForm: function () {
        var form = $("#GridFilterForm");
        var formObject = this.serializeObjectForGrid(form);
        var grid = this.getGrid();
        grid.refresh();

    },
    getGridId: function () {
        var grid = this.getGrid();
        return grid.element.id;
    },
    blockUI: function () {
        if (this.IsBlockUIEnabled) {
            utilities.ShowLoadingPanel();
        }
        utilities.SetVisibilityToTableActions(false);
    },
    unblockUI: function () {
        if (this.IsBlockUIEnabled) {
            utilities.HideLoadingPanel();
        }
        utilities.SetVisibilityToTableActions(true);
    },
    blockUIDisable: function () {
        this.IsBlockUIEnabled = false;
    },
    blockUIEnable: function () {
        this.IsBlockUIEnabled = true;
    },
    serializeObjectForGrid: function (form) {
        var o = {};
        var a = form.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                o[this.name] = o[this.name] + ',' + this.value || '';
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    },
    GoToElement: function (element) {
        $('html, body').animate({ scrollTop: element.offset().top }, 'slow');
    },
    GoToTop: function (element) {
        $("html, body").animate({ scrollTop: 0 }, 600);
    }
}

$(document).ready(function () {

    kendo.culture(culture)

    var isAbsoluteURI = new RegExp('^(?:[a-z]+:)?//', 'i');

    $(document).ajaxStart(function () {
        ajaxApp.blockUI();
    }).ajaxStop(function () {
        ajaxApp.unblockUI();
    }).ajaxSend(function (event, jqxhr, settings) {
        if (!isAbsoluteURI.test(this.url)) {
            //only add header to relative URLs
            jqxhr.setRequestHeader(HTTP_HEADER_NAME, antiForgeryToken);
        }
    }).ajaxComplete(function () {
        if ($.validator.unobtrusive != undefined)
        {
            $.validator.unobtrusive.parse(document);

            $("form :input").tooltipValidation({
                placement: "right"
            });
            //Todo:Caglar Bu iki method C# helper da handle edilmeli!Gecici cozum olarak koydum simdilik.
            SetAsteriks();
            SetMaxLength();
        }
    });

    $.validator.methods.date = function (value, element) {
        Globalize.culture("tr-TR");
        return this.optional(element) || Globalize.parseDate(value) !== null;
    }

    $("form input").tooltipValidation({
        placement: "right"
    });

    if (chosenEnabled) {
        $('.chosen-select').chosen({ case_sensitive_search: false, width: "95%", search_contains: true });
    }

    $.ajaxSetup({ cache: false, timeout: 2000000 });
});

function SetMaxLength() {

    $("input[data-val-range-max],input[data-val-length-max]").each(function (i, e) {
        var input = $(e);
        var maxlength = input.is("[data-val-range-max]")
            ? input.data("valRangeMax").toString().length
            : input.data("valLengthMax");
        input.attr("maxlength", maxlength);
    });
}

function SetAsteriks() {

    var $form = $('form');

    $('form').find("[data-val-required]").each(function (index) {

        var $input = $(this);

        if ($input.data("noasterix") != true) {

            var requiredAsterisk = "<span class=\"required\">*</span>";
            var id = $input.attr('id');
            var $label = $form.find("label[for='" + id + "']");
            //alert(id);
            if ($label.length > 0) {
                var html = $label.html() + "";
                if (html.indexOf(requiredAsterisk) <= 0) $label.html(html + requiredAsterisk);
            }

        }
    });

};

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function HighlightBooleanRecord(e) {

    var container = this.element;

    var cells = container.find("td[isbool=1]");

    for (var i = 0; i < cells.length; i++) {

        var cell = $(cells[i]);

        if (cell.html() == '1' || cell.html() == '0') {
            cell.html(HighlightBooleanRecordData(cell.html()));
        }
    }
}

function HighlightBooleanRecordData(data) {

    if (data == "1") {
        return "<img src='" + trueImage + "'>";
    }
    else if (data == "0") {
        return "<img src='" + falseImage + "'>";
    }
}

function getUrlVars(href) {
    var querystring = href.replace('?', '&').split('&');
    var queryObj = {};
    for (var i = 1; i < querystring.length; i++) {
        var name = querystring[i].split('=')[0];
        var value = querystring[i].split('=')[1];
        queryObj[name] = value;
    }
    return queryObj;
}

function isdefined(variable) {
    return (typeof (window[variable]) == "undefined") ? false : true;
}

$('body').on('click', "#CheckAllOnGrid", function (e) {
    var gridViewId = $(this).attr("data-checkbox-gridid");
    if ($('#CheckAllOnGrid').prop('checked') == false) {
        $('#' + gridViewId + ' input[type="checkbox"][name="checkedRecords"]').removeAttr('checked');
    }
    else {
        $('#' + gridViewId + ' input[type="checkbox"][name="checkedRecords"]').prop('checked', true);
    }

});

var ajaxRequest = {

    Call: function (url, data, successCallback, errorCallback) {
        $.ajax({
            url: url,
            type: 'post',
            cache: false,
            async: true,
            data: data,
            success: function (result) {
                if (successCallback) {
                    successCallback(result);
                }
            },
            error: function (result) {
                if (errorCallback) {
                    errorCallback(result);
                }
            }
        });
    }
};