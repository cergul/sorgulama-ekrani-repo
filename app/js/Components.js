
(function ($) {

    // Search Alphabetical
    $("#alphabetical").on("keyup",
        function () {
            var matcher = new RegExp($(this).val(), "gi");
            $(".app-box").show().not(function () {
                return matcher.test($(this).find("h2, h4").text());
            }).hide();
        });

    //Nav Collapsed
    $("#appcategories").on("show.bs.collapse",
        function () {
            $(".nav-tabs").addClass("nav-stacked");
        });

    $("#appcategories").on("hide.bs.collapse",
        function () {
            $(".nav-tabs").removeClass("nav-stacked");
        });

    // Confirm Modal
    $('a[name="confirmBtn"]').on("click",
        function (e) {
            var $form = $(this).closest("form");
            e.preventDefault();
            $("#confirm").modal({
                backdrop: "static",
            })
                .one("click",
                "#continue",
                function (e) {
                    $form.trigger("submit");
                });
        });

    // Toggle Sidebar
    $("#filterToggle").on("click",
        function () {
            $("#wrapperSidebar").toggleClass("active");
            $("#wrapperContent").toggleClass("col-md-12 col-md-10");
            $(this).html($(this).html() == '<i class="fo-icon-format-indent-increase"></i> Filtreleri Göster'
                ? '<i class="fo-icon-format-indent-decrease"></i> Filtreleri Gizle'
                : '<i class="fo-icon-format-indent-increase"></i> Filtreleri Göster');
        });

    // Toggle Sidebar
    //$('.filter-toggle').on('click', function () {
    //    $('#wrapperSidebar').toggleClass('active');
    //    $('#wrapperContent').toggleClass('col-md-12 col-md-10');


    //    if ($(this).attr("id") == "filterToggle") {
    //        $(this).html($(this).html() == 'Filtreleri Göster' ? 'Filtreleri Gizle' : 'Filtreleri Göster');
    //        $("#hide-sidebar").toggleClass("active show-sidebar");
    //        $("#filterToggle").toggleClass("active");
    //    } else if ($(this).attr("id") == "hide-sidebar") {
    //        $("#hide-sidebar").toggleClass("active show-sidebar");
    //        $("#filterToggle").toggleClass("active");
    //        $("#filterToggle").html($("#filterToggle").html() == 'Filtreleri Göster' ? 'Filtreleri Gizle' : 'Filtreleri Göster');
    //    }
    //}); 

    // Tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    // Popover Video Check
    $(function () {
        // $("#popoverResponsible").on("click", function () { $("#popoverResponsibleIcon").toggleClass("fo-popover-pencil fo-popover-close") }),
        $('#popoverResponsible').popover({
            placement: 'bottom',
            title: 'Servis Danışmanı Düzenle',
            html: true,
            content: $('#popoverContentResponsible').html()
        }),
            // $("#popoverStatus").on("click", function () { $("#popoverStatusIcon").toggleClass("fo-popover-pencil fo-popover-close") }),
            $('#popoverStatus').popover({
                placement: 'bottom',
                title: 'Durum Düzenle',
                html: true,
                content: $('#popoverContentStatus').html()
            })

        $('body').on('click', function (e) {
            $('[data-toggle="popover"]').each(function () {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
    });

    kendo.culture("tr-TR");

    // Date Picker
    $(".OpenDatePicker").kendoDatePicker();

    $(".OpenDatePicker").bind("focus", function () {
        $(this).data("kendoDatePicker").open();

    });

    // Clear Input

    $("#clearInput").click(function () {
        $(".k-datepicker input").val('');
    });

    // Carousel
    var totalItems = $('.item').length;
    var caption = $('.item > img').prop('alt');
    var currentIndex = $('div.active').index() + 1;
    $('.img-counter').html('' + currentIndex + ' / ' + totalItems + ' <i class="fa fa-circle"></i> ' + caption + '');

    $('#carousel').carousel({
        interval: false
    });

    $('#carousel').on('slide.bs.carousel', function () {
        currentIndex = $('div.active').index() + 1;
        $('.img-counter').html('' + currentIndex + ' / ' + totalItems + ' <i class="fa fa-circle"></i> ' + caption + '');
    });

    // Carousel Thumbnail Selection
    $('[id^=thumb-]').click(function () {
        var id_selector = $(this).attr("id");
        var id = id_selector.substr(id_selector.lastIndexOf("-") + 1);
        id = parseInt(id);
        $('#carousel').carousel(id);
        $('[id^=thumb-]').removeClass('active-thumb');
        $(this).addClass('active-thumb');
    });

    // Toggle
    //$(function () {
    //    $(".primary-card").on("click", function () {
    //        $("#togglePanel").slideToggle("100");
    //    });


    //    $(document).click(function (e) {
    //        if (!$(e.target).is('.primary-card')) {
    //            $('.primary-card-collapse').collapse('hide');
    //        }
    //    });
    //});

    // Primary Card Toggle
    $(document.body).click(function (e) {
        if (!$(e.target).is('.primary-card')) {
            $('#togglePanel').collapse('hide').on('click', '.panel-body', function (e) {
                e.stopPropagation();
            });;
        }
    });


    // Modal Height
    $(".modal-wide").on("show.bs.modal", function () {
        var height = $(window).height() - 200;
        $(this).find(".modal-body").css("max-height", height);
    });

    // Pagination
    $('.fo-pagination-button').twbsPagination({
        totalPages: 5,
        startPage: 1,
        visiblePages: 5,
        initiateStartPageClick: true,
        href: false,
        hrefVariable: '{{number}}',
        first: 'First',
        prev: 'Previous',
        next: 'Next',
        last: 'Last',
        loop: false,
        onPageClick: function (event, page) {
            $('.fo-pagination-active').removeClass('fo-pagination-active');
            $('#page' + page).addClass('fo-pagination-active');
        },
        paginationClass: 'pagination',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first',
        pageClass: 'page',
        activeClass: 'active',
        disabledClass: 'disabled'
    });

})(jQuery);





$(document).ready(function () {

    // Lightbox Image and Video
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1]
        },
        image: {
            tError: '<a href="%url%">bu görsel #%curr%</a> yüklenemedi.',
            titleSrc: function (item) {
                return item.el.attr('title') + '<small>Ford Otosan - Ek Bulgu</small>';
            }
        }
    });

    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

});

function customBoolEditor(container, options) {
    var guid = kendo.guid();
    $('<input class="k-checkbox" id="' + guid + '" type="checkbox" name="Discontinued" data-type="boolean" data-bind="checked:Discontinued">').appendTo(container);
    $('<label class="k-checkbox-label" for="' + guid + '">&#8203;</label>').appendTo(container);
}


// User Interface

// Active Tab Name
$('.fo-icon-keyboard-arrow-right').click(function () {
    var activeTabName = $(this).attr('title');
    $('#tabName').html(activeTabName);
});



$(function () {
    // Enables popover
    $("[data-toggle=popover]").popover();
});

// Add Lobour And Part Modal After Refresh
$('#AddLabourAndParts').on('hidden.bs.modal', function () {
    location.reload();
})


// Floating Scroll Selector
$(".fo-floatingscroll").floatingScroll();

// Referer Components
$(document).ready(function () {

    var enPlaceholder = "Please select...";
    var trPlaceholder = "Lütfen seçiniz...";

    $('.select-single').select2({
        //allowClear: true
    });
    $('.select-multiple').select2();
    $(".select-placeholder-single").select2({
        placeholder: enPlaceholder,
        allowClear: true
    });
    $(".select-placeholder-multiple").select2({
        placeholder: enPlaceholder,
        //allowClear: true
    });
});



