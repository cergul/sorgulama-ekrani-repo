﻿/***** PLUGIN AND FUNCTION INITIALIZATION  *****/

$(document).ready(function() {
    //TODO ÇAĞLAR - This is not reusuable
    $("#StockDsrGridBase").floatingScroll();
    $("select").kendoDropDownList();


});


window.onload = function() {

    //TODO ÇAĞLAR - This is not Generic
    // Plugin initialization
    $("#StockDsrGridBase").floatingScroll();
    $(".nav-tabs > li > a[data-tab-id='general']").parent().addClass("active");

    // BaseCar treeview case sensitive & Some wrong entry fixing patch.
    $('.k-in').text(function(index, text) {
        return text.toUpperCase().replace('FİESTA', 'FIESTA').replace('BINEK', 'BİNEK').replace('TICARI', 'TİCARİ');
    });



    //TODO - CAGAN - Metin Yılmaz will add these code block to framework
    var parentDiv = $("#GridFilterButton").parent();
    parentDiv.addClass("btn-group btn-group-sticky");
    parentDiv.attr("id", "filter-grid-button-group");
    var clearButton = $('<button/>', {
        html: '<i class="material-icons">clear_all</i> Temizle',
        id: 'clearButton',
        'class': 'btn fo-button-white-small',
        click: function() {
            utilities.ResetFilterForm();
        }
    });
    parentDiv.append(clearButton);
    //TODO END
};


/***** PLUGIN AND FUNCTION INITIALIZATION END *****/


/***** FIXED AND RELATIVE FILTER BUTTON FUNCTION *****/

$(window).on("scroll", function() {
    //TODO ÇAĞLAR - This is not Generic
    var scrollHeight = $(document).height();
    var scrollPosition = $(window).height() + $(window).scrollTop();
    var fixPlace;
    if ($("#branchList").is(":visible") === true) {
        fixPlace = "open";
    } else {
        fixPlace = "close";
    }

    StickyAndRelativeFilterButton(scrollHeight, scrollPosition, fixPlace);

    if ($(window).scrollTop() === 100) {
        $("#StockDsrGridBase").floatingScroll();
    }

});
//TODO ÇAĞLAR - This is not reusuable
function StickyAndRelativeFilterButton(scrollHeight, scrollPosition, fixPlace) {
    if (fixPlace === "open") {

        if (scrollPosition <= ($("#pool-statusses").position().top) + 370) {
            StickyFilterButton();
        } else {
            RelativeFilterButton();
        }
    } else {

        if (scrollPosition <= ($(".last-box").position().top) + 450) {
            StickyFilterButton();

        } else {
            RelativeFilterButton();
        }
    }
}

//TODO ÇAĞLAR - Refactor
function StickyFilterButton() {
    $("#filter-grid-button-group").removeClass("btn-group-static");
    $("#filter-grid-button-group").addClass("btn-group-sticky");
    $("a#hide-sidebar").removeClass("hide-sidebar-relative");
}

function RelativeFilterButton() {
    $("#filter-grid-button-group").addClass("btn-group-static");
    $("#filter-grid-button-group").removeClass("btn-group-sticky");
    $("a#hide-sidebar").addClass("hide-sidebar-relative");
}

/***** FIXED AND RELATIVE FILTER BUTTON FUNCTION ENDS *****/


/***** SELECT ALL FUNCTIONS *****/
var SELECTOBJ = {
    branchList: false,
    carSelect: false,
    modelYear: false,
    carStatus: false,
    controlCriteria: false,
    poolStatus: false,
    createdDays: false,
    carColor: false,
    carOption: false
};

//TODO ÇAĞLAR - This is not reusuable
$(document).delegate(".select-all", "click", function(e) {
    var parentName = $(this).parent()["0"].id;
    var parentId = "#" + $(this).parent()["0"].id + " input";
    var selectElement = this;

    switch (parentName) {
        case "branchList":
            selectFilterCheckboxes(selectElement, parentName, parentId, SELECTOBJ.branchList);
            break;
        case "car-select":
            selectFilterCheckboxes(selectElement, parentName, parentId, SELECTOBJ.carSelect);
            break;
        case "model-years":
            selectFilterCheckboxes(selectElement, parentName, parentId, SELECTOBJ.modelYear);
            break;
        case "car-statusses":
            selectFilterCheckboxes(selectElement, parentName, parentId, SELECTOBJ.carStatus);
            break;
        case "control-criteria":
            selectFilterCheckboxes(selectElement, parentName, parentId, SELECTOBJ.controlCriteria);
            break;
        case "pool-statusses":
            selectFilterCheckboxes(selectElement, parentName, parentId, SELECTOBJ.poolStatus);
            break;
        case "created-days":
            selectFilterCheckboxes(selectElement, parentName, parentId, SELECTOBJ.createdDays);
            break;
        case "car-color":
            selectFilterCheckboxes(selectElement, parentName, parentId, SELECTOBJ.carColor);
            break;
        case "car-option":
            selectFilterCheckboxes(selectElement, parentName, parentId, SELECTOBJ.carOption);
            break;
    }

});

//TODO ÇAĞLAR - This is not reusuable
function selectFilterCheckboxes(selectElement, parentName, parentId, selectFlag) {

    if (selectFlag) {
        $(parentId).each(function() {
            $(this).prop("checked", false);
            $(selectElement).html("Tümünü Seç");
        });

        switch (parentName) {
            case "branchList":
                SELECTOBJ.branchList = false;
                break;
            case "car-select":
                SELECTOBJ.carSelect = false;
                break;
            case "model-years":
                SELECTOBJ.modelYear = false;
                break;
            case "car-statusses":
                SELECTOBJ.carStatus = false;
                break;
            case "control-criteria":
                SELECTOBJ.controlCriteria = false;
                break;
            case "pool-statusses":
                SELECTOBJ.poolStatus = false;
                break;
            case "created-days":
                SELECTOBJ.createdDays = false;
                break;
            case "car-color":
                SELECTOBJ.carColor = false;
                break;
            case "car-option":
                SELECTOBJ.carOption = false;
                break;
        }
    } else {
        $(parentId).each(function() {
            $(this).prop("checked", true);
            $(selectElement).html("Tüm Seçimi Kaldır");
        });

        switch (parentName) {
            case "branchList":
                SELECTOBJ.branchList = true;
                break;
            case "car-select":
                SELECTOBJ.carSelect = true;
                break;
            case "model-years":
                SELECTOBJ.modelYear = true;
                break;
            case "car-statusses":
                SELECTOBJ.carStatus = true;
                break;
            case "control-criteria":
                SELECTOBJ.controlCriteria = true;
                break;
            case "pool-statusses":
                SELECTOBJ.poolStatus = true;
                break;
            case "created-days":
                SELECTOBJ.createdDays = true;
                break;
            case "car-color":
                SELECTOBJ.carColor = true;
                break;
            case "car-option":
                SELECTOBJ.carOption = true;
                break;
        }
    }

};
/***** SELECT ALL FUNCTIONS END *****/

//TODO ÇAĞLAR - This is not generic

/***** GRID TAB FUNCTIONS *****/
var generalTab = $(".nav-tabs > li > a[data-tab-id='general']");
var detailTab = $(".nav-tabs > li > a[data-tab-id='detail']");
$(".nav-tabs > li > a[data-tab-id='general']").click(function() {
    $("#StockDsrGridBase").floatingScroll();
    utilities.ShowLoadingPanel();

    setTimeout(function() {
        stockDsr.Functions.SetVisibilityToStockDsrGridFields(stockDsrGeneralGridFields, "show");
        stockDsr.Functions.SetVisibilityToStockDsrGridFields(stockDsrDetailGridFields, "hide");
        generalTab.addClass("active");
        detailTab.removeClass("active");
        utilities.HideLoadingPanel();
    }, 2000);

});

$(".nav-tabs > li > a[data-tab-id='detail']").click(function() {
    $("#StockDsrGridBase").floatingScroll();
    utilities.ShowLoadingPanel();
    setTimeout(function() {
        stockDsr.Functions.SetVisibilityToStockDsrGridFields(stockDsrGeneralGridFields, "hide");
        stockDsr.Functions.SetVisibilityToStockDsrGridFields(stockDsrDetailGridFields, "show");
        detailTab.addClass("active");
        generalTab.removeClass("active");
        utilities.HideLoadingPanel();
    }, 2000);

});

/***** Stock DSR Grid Total Page Info Updating Process *****/
$("span.k-pager-info").on('DOMSubtreeModified', function() {
    $(".total-page-info").html($(this).html());
});


/***** Kendo Numeric Textbox Destroy Wrapper and Rerender the UI of the Numeric Textbox *****/
function destroyNumeric(id) {
    var numeric = $("#" + id).data("kendoNumericTextBox");
    var origin = numeric.element.show();

    origin.insertAfter(numeric.wrapper);

    numeric.destroy();
    numeric.wrapper.remove();
}

/***** Min Max Numeric Textbox Destroy and Rerender functions *****/
$(function() {
    destroyNumeric("OtherFeatures_StockFinancingMinPrice");
    destroyNumeric("OtherFeatures_StockFinancingMaxPrice");

    destroyNumeric("CarFeatures_MinimumStockDayCount");
    destroyNumeric("CarFeatures_MaximumStockDayCount");

    destroyNumeric("CarFeatures_MinimumDayCountToSale");
    destroyNumeric("CarFeatures_MaximumDayCountToSale");


    $("#OtherFeatures_StockFinancingMinPrice").kendoNumericTextBox({
        placeholder: "Min",
        min: 0,
        max: 9999999,
        format: "#"
    });



    $("#OtherFeatures_StockFinancingMaxPrice").kendoNumericTextBox({
        placeholder: "Maks",
        min: 0,
        max: 9999999,
        format: "#"
    });



    $("#CarFeatures_MinimumStockDayCount").kendoNumericTextBox({
        placeholder: "Min",
        min: 0,
        max: 9999999,
        format: "#"
    });

    $("#CarFeatures_MaximumStockDayCount").kendoNumericTextBox({
        placeholder: "Maks",
        min: 0,
        max: 9999999,
        format: "#"
    });



    $("#CarFeatures_MinimumDayCountToSale").kendoNumericTextBox({
        placeholder: "Min",
        min: 0,
        max: 9999999,
        format: "#"
    });

    $("#CarFeatures_MaximumDayCountToSale").kendoNumericTextBox({
        placeholder: "Maks",
        min: 0,
        max: 9999999,
        format: "#"
    });

});


/***** Common Functions *****/

var utilities = {
    Fields: {
        TableActions: $("#dropdownUILeft")
    },
    ResetFilterForm: function() {
        //TODO : Metin - Form must take an id
        $("form")[0].reset();
    },
    ShowWarningMessage: function(message) {
        var messageList = {
            Messages: [message]
        };
        displayMessage(messageList, "warning");
    },
    // Successfull Filter Process - Scroll to Anchor Id with Animate
    ScrollToDiv: function(divId) {
        if (divId == null || divId == "") {
            divId = "wrapperContent";
        }
        $('html, body').animate({
            scrollTop: $("#" + divId).offset().top - 50
        }, 1000);
    },
    ShowErrorMessage: function(message) {
        var messageList = {
            Messages: [message]
        };
        displayMessage(messageList, "error");
    },
    ShowLoadingPanel: function() {
        var urlPath = location.pathname;
        urlPath += "/Content/img/loading.gif";
        $.blockUI({
            baseZ: 100003,
            //message: '<img src="' + blockUILoadingGifPath + '" />'
            message: '<img  src="' + urlPath + '" class="loading" width="100px"/>'

        });
    },
    HideLoadingPanel: function() {
        $.unblockUI();
    },
    //Opens Ifreame In A Modal
    OpenPopup: function(url, width, height) {
        width = width ? width : '600';
        height = height ? height : '600';
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

        var windowWidth = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var windowHeight = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((windowWidth / 2) - (width / 2)) + dualScreenLeft;
        var top = ((windowHeight / 2) - (height / 2)) + dualScreenTop;
        var newwindow = window.open(url, '', 'location=no , height=' + height + ',width=' + width + ', top = ' + top + ', left = ' + left);
        if (window.focus) { newwindow.focus() }
        return false;
    },
    HideGridColumn: function(grid, fieldName) {
        grid.hideColumn(fieldName);
    },
    ShowGridColumn: function(grid, fieldName) {
        grid.showColumn(fieldName);
    },
    SetVisibilityToTableActions: function(visibility) {

        if (this.Fields.TableActions == null || this.Fields.TableActions == undefined || this.Fields.TableActions.length < 1) {
            return;
        }
        if (visibility) {
            this.Fields.TableActions.prop('disabled', false);
        } else {
            this.Fields.TableActions.prop('disabled', true);
        }
    }
};

/***** END Common Functions *****/


/***** STOCK / DSR  *****/

var stockDsr = {
    Fields: {
        StockDsrGridPoolStatusField: $('.stockDsrGridPoolStatusField'),
        H1PoolStatus: $('.poolStatussesCheckbox > input[type=checkbox][value=1]'),
        H2PoolStatus: $('.poolStatussesCheckbox > input[type=checkbox][value=2]'),
        H3PoolStatus: $('.poolStatussesCheckbox > input[type=checkbox][value=3]'),
        H4PoolStatus: $('.poolStatussesCheckbox > input[type=checkbox][value=4]'),
        OtherBranchCheckbox: null,
        StockCheckbox: null,
        DsrCheckBox: null,
        StockDsrGrid: null
    },
    Div: {
        CarColorAndOptionDiv: $('#CarColorAndOptionDiv'),
        BranchListDiv: $("#branchList"),
        CarStatusses: $("#car-statusses"),
        ControlCriteria: $("#control-criteria"),
        StockDayCount: $("#stockDayCount"),
        MinDayCountToSale: $("#minDayCountToSale"),
        CreatedDays: $("#created-days"),
        StockFinancingPrices: $("#stockFinancingPrices"),
        StockDsrGrid: $("#StockDsrGridBase")
    },
    Functions: {
        GetCarOptionAndColorByBaseCar: function() {
            var ajaxRequestData = {
                carModelList: filterFormPartialObject.carModelList
            };

            ajaxRequest.Call(filterFormPartialObject.getCarOptionandColorURL, ajaxRequestData, this.BindCarOptionAndBaseCarView);
        },
        BindCarOptionAndBaseCarView: function(partialViewResult) {
            stockDsr.Div.CarColorAndOptionDiv.html(partialViewResult);
        },
        ChangeBranchListVisibilityByStockPlace: function() {
            if (stockDsr.Fields.OtherBranchCheckbox.prop("checked")) {
                stockDsr.Div.BranchListDiv.removeClass("hidden");

            } else if (stockDsr.Fields.OtherBranchCheckbox.prop("checked") == false && stockDsr.Div.BranchListDiv.hasClass("hidden") == false) {
                stockDsr.Div.BranchListDiv.addClass("hidden");
            }
        },
        CheckSearchPlacesesVisibilityByLoggedUser: function(isResetForm) {
            if (filterFormPartialObject.isHeadOffice) {
                var otherBranchCheckBoxLabel = $(stockDsr.Fields.OtherBranchCheckbox[0].parentNode).find(".checkbox-label");
                if (otherBranchCheckBoxLabel) {
                    otherBranchCheckBoxLabel.html("Bayi listesi");
                }
            }
            if (stockDsr.Fields.StockCheckbox.prop("checked") || stockDsr.Fields.DsrCheckBox.prop("checked") || isResetForm) {
                stockDsr.Functions.DisableOtherBranchSearchPlace();
            } else if (stockDsr.Fields.OtherBranchCheckbox.prop("checked")) //DSR Search - Other Branch
            {
                stockDsr.Functions.DisableStockAndDsrSearchPlace();
                this.StockDsrHideFields(false);
                return;
            } else {
                stockDsr.Fields.StockCheckbox.prop('disabled', false);
                stockDsr.Fields.DsrCheckBox.prop('disabled', false);
                stockDsr.Fields.OtherBranchCheckbox.prop('disabled', false);
            }

            this.StockDsrHideFields(true);
        },
        StockDsrHideFields: function(visibility) {
            if (visibility) {
                stockDsr.Div.CarStatusses.show();
                stockDsr.Div.ControlCriteria.show();
                stockDsr.Div.StockDayCount.show();
                stockDsr.Div.MinDayCountToSale.show();
                stockDsr.Div.CreatedDays.show();
                stockDsr.Div.StockFinancingPrices.show();
            } else {
                stockDsr.Div.CarStatusses.hide();
                stockDsr.Div.ControlCriteria.hide();
                stockDsr.Div.StockDayCount.hide();
                stockDsr.Div.MinDayCountToSale.hide();
                stockDsr.Div.CreatedDays.hide();
                stockDsr.Div.StockFinancingPrices.hide();
            }
        },
        CheckedNodeIds: function(nodes, checkedNodes, isBaseCar) {
            for (var i = 0; i < nodes.length; i++) {
                //If all child of node are not selected or check removed, checked property being 'undefined'
                if (nodes[i].checked || nodes[i].checked == undefined) {
                    if (nodes[i].id.indexOf("Vehicle") > 0) {
                        if (nodes[i].hasChildren) {
                            this.CheckedNodeIds(nodes[i].children.view(), checkedNodes);
                        }
                    } else if (nodes[i].id.indexOf("Model") > 0) {
                        var carModel = {
                            CarId: nodes[i].id.replace("-Model", ""),
                            BaseCarList: []
                        };
                        if (nodes[i].hasChildren) {
                            this.CheckedNodeIds(nodes[i].children.view(), carModel, true);

                            if (carModel.BaseCarList.length > 0) {
                                checkedNodes.push(carModel);
                            }
                        }
                    } else if (nodes[i].id.indexOf("Base") > 0 && nodes[i].checked) {
                        checkedNodes.BaseCarList.push({
                            Value: nodes[i].id.replace("-Base", "")
                        });
                    }
                }
            }
        },
        CheckIndeterminateNodes: function(treeId) {
            $('#' + treeId + ' :checkbox').each(function() {
                if (this.indeterminate) { this.checked = true; }
            });
        },
        HideOrShowBaseCars: function(visiblity) {
            $("#Cars .k-item").closest(".k-item").find("li").css("display", visiblity);
        },
        StockDsrRefreshGrid: function() {
            var stockDsrGrid = stockDsr.Div.StockDsrGrid.data("kendoGrid");

            if (stockDsrGridId.attr("filterKey")) {
                stockDsrGrid.dataSource.read();
                stockDsrGridId.removeAttr("filterKey");
            }
        },
        OnCarsTreeCheck: function(e) {
            var treeView = $("#Cars").data("kendoTreeView");
            filterFormPartialObject.carModelList = [];
            this.CheckedNodeIds(treeView.dataSource.view(), filterFormPartialObject.carModelList);
            this.GetCarOptionAndColorByBaseCar();
            this.CheckIndeterminateNodes('Cars');
        },
        StockDsrFilterGridByKey: function(searchQuery) {
            var stockDsrGrid = stockDsr.Div.StockDsrGrid.data("kendoGrid");
            var searchQueryRegEx = new RegExp(searchQuery, 'i');
            if (stockDsrGrid.dataSource.data()) {

                var filteredData = stockDsrGrid.dataSource.data().filter(
                    function(record) {
                        if (record.MODEL_NAME.search(searchQueryRegEx) > -1 ||
                            record.RowOrderCarPursuitColumn.search(searchQueryRegEx) > -1 ||
                            record.BaseCarColumn.search(searchQueryRegEx) > -1 ||
                            record.CarModelYearColumn.search(searchQueryRegEx) > -1 ||
                            record.ColorAndOptionColumn.search(searchQueryRegEx) > -1 ||
                            record.StockAgeColumn.search(searchQueryRegEx) > -1 ||
                            record.WholeSaleColumn.search(searchQueryRegEx) > -1 ||
                            record.PriceAndDiscountCodeColumn.search(searchQueryRegEx) > -1 ||
                            record.DownPaymentColumn.search(searchQueryRegEx) > -1 ||
                            record.BranchNameColumn.search(searchQueryRegEx) > -1 ||
                            record.RF_STS_SIRANO.search(searchQueryRegEx) > -1 ||

                            record.DEALER_ORDER_NUMBER.search(searchQueryRegEx) > -1 ||
                            record.ENTITY_NAME.search(searchQueryRegEx) > -1 ||
                            record.VINNUMBER.search(searchQueryRegEx) > -1 ||
                            record.MODELYEAR.search(searchQueryRegEx) > -1 ||
                            record.COLOR_CODE.search(searchQueryRegEx) > -1 ||
                            record.OPTION_DSC.search(searchQueryRegEx) > -1 ||
                            record.PoolStatusColumn.search(searchQueryRegEx) > -1 ||
                            record.StatussesColumn.search(searchQueryRegEx) > -1 ||
                            record.LastCheck.search(searchQueryRegEx) > -1 ||
                            record.HasNoSevk.search(searchQueryRegEx) > -1 ||

                            record.FsaStatus.search(searchQueryRegEx) > -1 ||
                            record.SAYAC.search(searchQueryRegEx) > -1 ||
                            record.PRODUCTION_MONTH.search(searchQueryRegEx) > -1 ||
                            record.TARGET_DELIVERY_DATE.search(searchQueryRegEx) > -1 ||
                            record.DT_EWS.search(searchQueryRegEx) > -1 ||
                            record.SPECIAL_PRICE_REASON_CODE.search(searchQueryRegEx) > -1 ||
                            record.DP_HO_CONFIRM_CASE.search(searchQueryRegEx) > -1 ||
                            record.DP_PRICE.search(searchQueryRegEx) > -1 ||
                            record.DP1.search(searchQueryRegEx) > -1 ||
                            record.ORDER_TYPE.search(searchQueryRegEx) > -1 ||

                            record.COMMERCIAL_MARKET.search(searchQueryRegEx) > -1 ||
                            record.BRAND.search(searchQueryRegEx) > -1 ||
                            record.PHYSICAL_STATUS.search(searchQueryRegEx) > -1 ||
                            record.LOCATION_STATUS.search(searchQueryRegEx) > -1 ||
                            record.CUST_ORDER_NUMBER.search(searchQueryRegEx) > -1 ||
                            record.SALES_TYPE.search(searchQueryRegEx) > -1 ||
                            record.STOCK_ENTRY_DATE.search(searchQueryRegEx) > -1 ||
                            record.SALES_MANAGER_NOTE.search(searchQueryRegEx) > -1 ||
                            record.FO_NOTE.search(searchQueryRegEx) > -1 ||
                            record.VEHICLE_STATUS.search(searchQueryRegEx) > -1 ||

                            record.FLEET_ASSIGN_DATE.search(searchQueryRegEx) > -1 ||
                            record.IS_SVO.search(searchQueryRegEx) > -1 ||
                            record.EKIPMAN_INDIKATURU.search(searchQueryRegEx) > -1 ||
                            record.INVOICEDATE.search(searchQueryRegEx) > -1 ||
                            record.AGREEMENT_NUMBER.search(searchQueryRegEx) > -1 ||
                            record.DAY_LIMIT.search(searchQueryRegEx) > -1 ||
                            record.WSNUMBER.search(searchQueryRegEx) > -1 ||
                            record.WS_MATBU_NO.search(searchQueryRegEx) > -1 ||
                            record.TARGET_PROD_DATE.search(searchQueryRegEx) > -1 ||
                            record.AGRID.search(searchQueryRegEx) > -1 ||


                            record.AGRNUM.search(searchQueryRegEx) > -1 ||
                            record.GUMRUK.search(searchQueryRegEx) > -1 ||
                            record.GUMRUKBYNTAR.search(searchQueryRegEx) > -1 ||
                            record.GUMRUKBYNNO.search(searchQueryRegEx) > -1 ||
                            record.MAKBUZNO.search(searchQueryRegEx) > -1 ||
                            record.MAKBUZTAR.search(searchQueryRegEx) > -1 ||
                            record.WAYBILLID.search(searchQueryRegEx) > -1 ||
                            record.WAYBILLDATE.search(searchQueryRegEx) > -1 ||
                            record.SalesNoteColumn.search(searchQueryRegEx) > -1) {
                            return record;
                        }

                    });

                stockDsrGrid.dataSource.data(filteredData);

                stockDsr.Div.StockDsrGrid.attr("filterKey", searchQuery);
            }

        },
        SetGridPoolStatusFieldDisable: function(isDisabled) {
            stockDsr.Fields.StockDsrGridPoolStatusField.prop("disabled", isDisabled);
        },
        SetPoolStatusesFilterVisibilityByStockType: function() // H1 and H2 Pool Statusses are useless on Other Branch Search
            {
                var isDisabled = false;
                if (stockDsr.Fields.OtherBranchCheckbox && stockDsr.Fields.OtherBranchCheckbox.prop("checked")) {
                    isDisabled = true;
                } else {
                    isDisabled = false;
                }
                stockDsr.Fields.H1PoolStatus.prop("disabled", isDisabled);
                stockDsr.Fields.H2PoolStatus.prop("disabled", isDisabled);
            },
        OpenRealocationPopup: function(button, urlDomain) {
            var clickedRowId = $(button).attr("rowId");
            var clickedRowDealerName = $(button).attr("dealerName");
            var clickedRowDealerCode = $(button).attr("dealerCode");
            var clickedRowPoolStatus = $(button).attr("poolStatus");
            var clickedRowDealerOrderNumber = $(button).attr("dealerOrderNumber");
            var clickedRowDayCountToSale = $(button).attr("dayCountToSale");
            var clickedRowDealerCodeId = $(button).attr("dealerCodeId");
            var clidkedRowCanRealocation = $(button).attr("canRealocation");
            var validationMessage = $(button).attr("validationMessage");

            if (validationMessage && clidkedRowCanRealocation != "true") {
                utilities.ShowWarningMessage(validationMessage);
                return false;
            }
            var url = urlDomain + 'RealokasyonTalebi.aspx?dealername=' + clickedRowDealerName + '&dealerordernumber_=' + clickedRowDealerOrderNumber + '&dealercode_=' + clickedRowDealerCode + '&poolstatus=' + clickedRowPoolStatus + '&dealercodeid_=' + clickedRowDealerCodeId + '&sayac_=' + clickedRowDayCountToSale;
            OpenIframeModal(url, 1000, 640);
        },
        ArrangeGridColumnsByStockType: function() {
            //If is Other Branch Search
            if (stockDsr.Fields.OtherBranchCheckbox && stockDsr.Fields.OtherBranchCheckbox.prop("checked")) {
                utilities.ShowGridColumn(stockDsr.Fields.stockDsrGrid, "RealocationColumn");
            } else {
                utilities.HideGridColumn(stockDsr.Fields.stockDsrGrid, "RealocationColumn");
            }
        },
        SetVisibilityToStockDsrGridFields: function(fieldList, visibility) {

            if (fieldList == null || visibility == null || stockDsrGrid == null) {
                return;
            }
            var gridVisibilityFunction;
            if (visibility == "show") {
                gridVisibilityFunction = utilities.ShowGridColumn;
            } else {
                gridVisibilityFunction = utilities.HideGridColumn;
            }

            $.each(fieldList, function(key, value) {
                gridVisibilityFunction(stockDsrGrid, value);
            });
        },
        StockTypesCheckboxesOnChange: function(isResetForm) {
            stockDsr.Functions.CheckSearchPlacesesVisibilityByLoggedUser(isResetForm);
            stockDsr.Functions.ChangeBranchListVisibilityByStockPlace();
            stockDsr.Functions.SetPoolStatusesFilterVisibilityByStockType();
        },
        DisableOtherBranchSearchPlace: function() {
            stockDsr.Fields.OtherBranchCheckbox.prop('disabled', true).prop('checked', false);
            stockDsr.Fields.StockCheckbox.prop('disabled', false);
            stockDsr.Fields.DsrCheckBox.prop('disabled', false);;
        },
        DisableStockAndDsrSearchPlace: function() {
            stockDsr.Fields.OtherBranchCheckbox.prop('disabled', false);
            stockDsr.Fields.StockCheckbox.prop('disabled', true).prop('checked', false);
            stockDsr.Fields.DsrCheckBox.prop('disabled', true).prop('checked', false);
        }
    }
};

/***** END STOCK / DSR *****/

// Double-Click to select a row from the Grid Table
$("table").on("dblclick", "tr", function() {
    var checkbox = $(this).find("input[type='checkbox']");

    var noteButton = $(this).find("button");

    if (checkbox.prop("checked")) {
        checkbox.trigger("click");
        $(this).removeClass("k-state-selected");
        noteButton.removeClass("fo-button-white");
        noteButton.addClass("fo-button");
    } else {
        checkbox.trigger("click");
        $(this).addClass("k-state-selected");
        noteButton.removeClass("fo-button");
        noteButton.addClass("fo-button-white");
    }

});

// Filter option on reseller section.
function FilterKendoReseller() {

    var input, filter, ul, li, a, i;
    input = $('#search-branch-list');
    filter = input.val().toLowerCase();
    ul = $("div[data-name='branch-list'] > ul");
    li = $("div[data-name='branch-list'] > ul > li");

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("label")[0];
        if (a.innerHTML.toLowerCase().indexOf(filter) > -1) {
            li[i].style.display = "";

        } else {
            li[i].style.display = "none";
        }

        if ($(ul).find("li").is(":visible") == true) {
            $("div#no-result-branch").addClass("hidden");
        } else {
            $("div#no-result-branch").removeClass("hidden");
        }
    }
}

// Filter Option function Getting treeview and search input parameters.
function FilterKendoOption(treeName, filterInput) {
    var filterText = filterInput.toUpperCase();
    if (filterText !== "") {
        $(treeName + " .k-group .k-group .k-in").closest("li").hide();
        $(treeName + " .k-group").closest("li").hide();
        $(treeName + " .k-in:contains(" + filterText + ")").each(function() {
            $(this).parents("ul, li").each(function() {
                var treeView = $(treeName).data("kendoTreeView");
                treeView.expand($(this).parents("li"));
                $(this).show();
            });
        });
        $(treeName + " .k-group .k-in:contains(" + filterText + ")").each(function() {
            $(this).parents("ul, li").each(function() {
                $(this).show();
            });
        });


    } else {
        $(treeName + " .k-group").find("li").show();
        var nodes = $(treeName + " > .k-group > li");
        var treeView = $(treeName).data("kendoTreeView");
        treeView.collapse(".k-item");
        $("div#no-result-search-car").addClass("hidden");

    }

    if ($(treeName + " .k-group .k-group").find("li").is(":visible") == true) {
        $("div#no-result-search-car").addClass("hidden");
    } else {
        $("div#no-result-search-car").removeClass("hidden");
    }

}